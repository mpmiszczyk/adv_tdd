#pragma once



class Complex{

	private:
		double _re, _im;

	public:
		Complex ( double real, double immaginary):
			_re(real),
			_im(immaginary){ };


		bool operator== ( const Complex & a) const{
			return _re == a._re && _im == a._im;
		}
		bool operator!= ( const Complex & a) const{
			return _re != a._re || _im != a._im;
		}


		Complex add ( const Complex & an ) const {
			return Complex ( _re + an._re,  _im + an._im);
		}

		Complex multiply ( const Complex & an ) const {
			return Complex ( 
							_re * an._re  -  _im * an._im,
							_im * an._im  +  _re * an._im );
		}

		Complex divide ( const Complex & an ) const {
			double denominator = an._re*an._re + an._im*an._im;

			return Complex ( 
							(_re * an._re  -  _im * an._im) / denominator,
							(_im * an._im  +  _re * an._im) / denominator);
		}

};
