#include "gtest/gtest.h"

#include "Complex.h"

TEST( ComplexShould, be_able_to_create ){

	Complex c ( 0,0);

}

TEST( ComplexShould, be_able_to_tell_if_same ){

	ASSERT_EQ ( Complex( 0,0), Complex( 0,0)) << "Porownywanie dwoch obiektow przez operator==";
	ASSERT_EQ ( Complex( 4,1), Complex( 4,1));
	ASSERT_EQ ( Complex( 0,3), Complex( 0,3));
}



TEST( ComplexShould, be_able_to_tell_if_diferent ){

	ASSERT_NE ( Complex( 0,1), Complex( 0,0)) << "Porownywanie dwoch obiektow przez operator!=";
	ASSERT_NE ( Complex( 1,0), Complex( 0,0));
	ASSERT_NE ( Complex( 1,3), Complex(-1,3));
}



TEST( ComplexShould, be_able_to_add ){

	ASSERT_EQ ( Complex( 4,3),  Complex(3,1).add(Complex(1,2)) );
}


TEST( ComplexShould, be_able_to_do_complex_calculation ){

	Complex wynik = Complex ( 1 , 3)
							.multiply( Complex( 2, -4))
							.divide( Complex ( 2, 1))
							.add ( Complex( 2, 6 ));

	ASSERT_EQ ( Complex( 8,4),  wynik );
}
