#include "gtest/gtest.h"

#include <math.h>


TEST( basic, will_succeed){
	SUCCEED();
}


TEST( basic, do_math){
	ASSERT_TRUE( 0 < 1 );

	ASSERT_EQ( 4, 2+2);

	ASSERT_NE( 8, 2+2*2);

}


TEST( basic, can_float_and_swim){
	ASSERT_EQ( 4.0, 2*2.0);

	ASSERT_EQ( 1.5, 6.0/4);

	ASSERT_NE( 3.14, M_PI) << "Za duze zaokaglenie";

	ASSERT_NEAR( 3.1 , M_PI , 1) << "Teraz juz lepiej";
}


