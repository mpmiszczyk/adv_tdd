#include "gmock/gmock.h"

#include "AbstractDB.h"

class MockBase: public AbstractDB {
	public:

		MOCK_METHOD2 (setValue, void ( std::string key, std::string value)) ;
		//virtual void setValue( std::string key, std::string value) = 0;

		MOCK_METHOD1 ( get , std::string ( std::string key) );
		//virtual std::string get( std::string key)  = 0;
		//virtual std::string get( std::string key) throw (std::exception) = 0;


};
