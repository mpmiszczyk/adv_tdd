#pragma once

#include "AbstractDB.h"

#include <map>
#include <exception>



class DataBase : public AbstractDB{

	std::map< std::string, std::string > _data;

	public:

		virtual void setValue( std::string key, std::string value){
			_data[ key ] = value;

		}

		virtual std::string get( std::string key) throw( std::exception) {
			if ( _data.count( key) ){
				return _data[ key];

			} else {
				throw std::exception();
			}
		}

		virtual ~DataBase(){};

};
