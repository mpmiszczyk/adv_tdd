#include "gtest/gtest.h"

#include "Cache.h"

#include "AbstractDB_mock.cpp"


TEST( InMemCacheShoul, store_values) {

	MockBase mock;
	Cache db = Cache( &mock );

	db.setValue( "key" , "value");
	ASSERT_EQ( "value" , db.get( "key") );

}


TEST( InMemCacheShoul, get_values_by_key){

	MockBase mock;
	Cache db = Cache( &mock );

	db.setValue( "key" , "value");
	db.setValue( "key2" , "value2");

	ASSERT_EQ( "value" , db.get( "key") );
}


TEST( InMemCacheShoul, get_values_from_database){

	using ::testing::Return;

	MockBase mock;
	EXPECT_CALL( mock, get( "key") )
		.Times(1)
		.WillOnce( Return ("value"));


	Cache db = Cache( &mock );

	ASSERT_EQ( "value" , db.get( "key") );
}




TEST( InMemCacheShoul, handle_not_existing_keys){

	using ::testing::_ ;
	using ::testing::Throw ;

	MockBase mock;
	EXPECT_CALL( mock, get( _ ) )
		.WillOnce(Throw( std::exception() ));


	Cache db = Cache( &mock );


	ASSERT_ANY_THROW(
			db.get( "key"); );
}




TEST( InMemCacheShoul, calls_data_base_to_save_value){

	using ::testing::AtLeast;

	// ASSERT DataBase.setV was called once
	MockBase mock;
	EXPECT_CALL( mock , setValue ("key", "value") )
		.Times( AtLeast(1));

	Cache db = Cache( &mock );


	db.setValue( "key" , "value");

}


TEST( InMemCacheShoul, not_call_base_if_key_was_set_before){

	using ::testing::_;
	// ASSERT DataBase.get was not called
	MockBase mock;
	EXPECT_CALL( mock , get ( _ ) )
		.Times( 0 );

	Cache db = Cache( &mock );

	db.setValue( "key" , "value");
	db.get( "key");
	db.get( "key");

}


TEST( InMemCacheShoul, calls_data_base_exacly_once_to_get_value){

	using ::testing::_;
	// ASSERT DataBase.get was called only once
	MockBase mock;
	EXPECT_CALL( mock , get ( _ ) )
		.Times( 1 );

	Cache db = Cache( &mock );

	db.get( "key");
	db.get( "key");

}




TEST( InMemCacheShoul, DISABLED_calls_data_base_exacly_once_for_each_key){

	MockBase mock;
	Cache db = Cache( &mock );


	db.setValue( "key" , "value");
	db.setValue( "key2" , "value2");
	// ASSERT DataBase.set was called two times

	db.get( "key");
	db.get( "key");
	db.get( "key2");
	db.get( "key2");

	// ASSERT DataBase was called once with "key" and once with "key2"
}











