#pragma once
#include "AbstractDB.h"

#include <map>

class Cache: public AbstractDB{

	AbstractDB* _db;
	std::map< std::string, std::string> _cache;

	public:
		Cache( AbstractDB* db ):
			_db( db){};

		virtual void setValue( std::string key, std::string value){
			_cache[key] = value;
			_db->setValue( key, value);
		}

		virtual std::string get( std::string key) throw( std::exception) {
			if ( _cache.count(key) ){
				return _cache[key];
			} else {
				_cache[key] = _db->get (key);
				return _cache[ key];
			}
		}
};



