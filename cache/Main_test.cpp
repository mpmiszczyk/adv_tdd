#include "gtest/gtest.h"

#include "DataBase.h"

TEST( Main, idea){

	DataBase db;

	db.setValue( "key" , "value");

	ASSERT_EQ( "value" , db.get( "key") );
}




TEST( DataBaseShould, store_values){

	DataBase db;

	db.setValue( "key" , "value");
	ASSERT_EQ( "value" , db.get( "key") );
}

TEST( DataBaseShould, get_values_by_key){

	DataBase db;

	db.setValue( "key" , "value");
	db.setValue( "key2" , "value2");

	ASSERT_EQ( "value" , db.get( "key") );
}



TEST( DataBaseShould, handle_not_existing_keys){

	DataBase db;

	ASSERT_ANY_THROW(
			db.get( "key"); );
}






