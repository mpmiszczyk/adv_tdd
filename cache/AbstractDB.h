#pragma once

#include <string>

class AbstractDB{

	public:

		virtual void setValue( std::string key, std::string value) = 0;

		virtual std::string get( std::string key)  = 0;

		virtual ~AbstractDB(){};

};
