#pragma once

#include "Fit.h"
#include <list>
#include "Point.h"
#include "Result.h"


class Fitter {

	private:
		Fit* _fit;
		std::list<Point> _data;
		Result* _result;


	public:
		Fitter();
		~Fitter();

		void set_fit_type( Fit* fit);
		void set_data( std::list<Point> data);
		void fit();
		Result get_result();

};
