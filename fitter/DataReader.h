#pragma once

#include <iostream>
#include <list>
#include "Point.h"

class DataReader{

	private:
		int _size;
		std::list<Point> _data;



	public:
		DataReader( int size);


		void read_data ( std::istream& istream);
		std::list<Point> get();

};
