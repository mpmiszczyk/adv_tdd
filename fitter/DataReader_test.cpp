#include "gtest/gtest.h"

#include "DataReader.h"
#include "Array.h"

TEST( DataReaderShould, return_all_lines){

	std::stringstream input;
	input << "1 ; 1" << std::endl
		<< "2 ; 2 " << std::endl
		<< "3 ; 3 " << std::endl;


	DataReader d(Array::size);
	d.read_data( input );

	auto data = d.get();
	ASSERT_EQ( 3Lu, data.size() );

}



TEST( DataReaderShould, handle_one_number){

	std::stringstream input;
	input << "0 ; 0" << std::endl;

	DataReader d(Array::size);
	d.read_data( input );

	auto data = d.get();
	ASSERT_EQ( 1Lu, data.size() );
}
