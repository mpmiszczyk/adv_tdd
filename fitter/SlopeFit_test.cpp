#include "gtest/gtest.h"

#include "SlopeFit.h"


TEST( SlopeFitShould, fit_one_number){

	std::list<Point> data;
	data.push_back( Point(1,1));

	SlopeFit fit;

	Result * result = fit.fit( data );

	ASSERT_EQ( Result( 1), *(result) );
}


TEST( SlopeFitShould, fit_two_numbers){

	std::list<Point> data;
	data.push_back( Point(1,1));
	data.push_back( Point(2,2));

	SlopeFit fit;

	Result * result = fit.fit( data );

	ASSERT_EQ( Result( 1), *(result) );
}





TEST( SlopeFitShould, DISABLED_fit_zero){

	std::list<Point> data;
	data.push_back( Point(0,0));

	SlopeFit fit;

	Result * result = fit.fit( data );

	ASSERT_EQ( Result( 1), *(result) );
}


TEST( SlopeFitShould, DISABLED_strange_numbers){

	std::list<Point> data;
	data.push_back( Point(1,1));
	data.push_back( Point(-1,1));

	SlopeFit fit;

	Result * result = fit.fit( data );

	ASSERT_EQ( Result( 1), *(result) );
}


