//Tego pliku nie można modyfikowaæ
//
//Proszę napisa program wykorzystujący hierachię z poprzednich zajęć
//Proszę dopisać obsługę błędów tak jak poniżej podano
//Proszę deklarwać funkcje odpowiednio pod względem obsługi wyjątków
//
//Brak błędów kompilacji przy zdefiniwanych COMPILE_ERROR1 rownież dyskwalifikuje zadanie!!!
//
//Slope fit:
//a = sum_1_n(x_i * y_i)/sum_1_n(x_i^2)
//Fit liniowy: //opcjonalnie ale ma być przewidziany
//http://www.statisticshowto.com/articles/how-to-find-a-linear-regression-equation/
#ifndef TESTING


#include "DataReader.h"
#include "Array.h"
#include "Fitter.h"

#include "SlopeFit.h"
#include "LinearFit.h"

#include <iostream>


int main(int , char** )
{


	try
	{
		DataReader d(Array::size); // maksymalnie czyta 100 par jak więcej to odpowiedni wyjątek
		d.read_data(std::cin); //read data from cin format x1; y1\nx2; y2\n...
		//cin.eof() przdatne do wykrycia końca czytania (znaku końca pliku)

		Fitter f;

		f.set_data(d.get());

		f.set_fit_type(new SlopeFit);
		f.fit();
		f.get_result().print(std::cout);

		f.set_fit_type(new LinearFit);
		f.fit();
		f.get_result().print(std::cout);
	}



	catch ( Fit::Exception& e) // 1. zły format danych (1a brak do pary, 1b - zły format), 2. nie wystaraczjąca liczba punktów
	{
		Fit::ExceptionHandler::print(e); // wypisuje błąd na cout
	}
	catch ( std::exception& e)  //np. niemozliwosc zaalokowanie pamieci
	{
		Fit::ExceptionHandler::print(e); // wypisuje błąd na cout
	}
	catch (...)
	{}	


	return 0;
}



#else
#include "gtest/gtest.h"
int main(int argc, char** argv) {

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

#endif

