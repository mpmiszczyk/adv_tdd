#include "gtest/gtest.h"

#include "Point.h"

TEST( TestPoint, can_compare_two_points){
	ASSERT_TRUE ( Point(1,2) ==  Point( 1,2) );
	ASSERT_FALSE ( Point(0,0) ==  Point( 1,2) );
	ASSERT_EQ ( Point(1,2),  Point( 1,2) );


	ASSERT_NE ( Point ( 0,0), Point( 0,1));
	ASSERT_NE ( Point ( 1,2), Point( 3,4));
}


TEST( TestPoint, create_from_string){
	ASSERT_EQ( Point(0,0) , Point::FromLine( " 0;0 ") );
	ASSERT_EQ( Point(1,2) , Point::FromLine( " 1;2 ") );
	ASSERT_EQ( Point(3,567) , Point::FromLine( " 3  ;  567  ") );
}


TEST( TestPoint, empty_string_causes_error){

	ASSERT_THROW( {
			Point::FromLine("");
			},	Point::BadFormat );
}

TEST( TestPoint, bad_format_causes_error){

	ASSERT_THROW( {
			Point::FromLine(";");
			},	Point::BadFormat );

	ASSERT_THROW( {
			Point::FromLine(" 1 , 2");
			},	Point::BadFormat );

	ASSERT_THROW( {
			Point::FromLine(" 1.2 ; 2");
			},	Point::BadFormat );
}

