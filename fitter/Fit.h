#pragma once

#include <exception>
#include <iostream>
#include "Result.h"
#include "Point.h"
#include <list>


class Fit{

	protected:
		Fit(){};

	public:
		virtual ~Fit(){};


		virtual Result* fit( std::list<Point> data) = 0;

		class Exception: public std::exception {
		};

		class ExceptionHandler {
			public:
				static void print ( std::exception& e){
					std::cout << e.what();
				};
		};

};
