#include "DataReader.h"


DataReader::DataReader( int size):
	_size( size){};

void DataReader::read_data ( std::istream& istream){
	std::string line = "";

	while (std::getline( istream, line) ) {
		
		_data.push_back( Point::FromLine( line ));
	}

};

std::list<Point> DataReader::get(){
	return _data;
};

