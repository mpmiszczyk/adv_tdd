#include "gtest/gtest.h"

#include "DataReader.h"
#include "Array.h"
#include "Fitter.h"

#include "SlopeFit.h"
#include "LinearFit.h"

#include <sstream>

TEST( MainFunctionality, success){
	SUCCEED() << "It's Alive";
}




TEST( MainFunctionality, basic_fitting_for_slope_fitter){

	std::stringstream input, output;
	input << "1 ; 1" << std::endl
		<< "2 ; 2 " << std::endl
		<< "3 ; 3 " << std::endl;



	DataReader d(Array::size);
	d.read_data( input );

	Fitter f;

	f.set_data(d.get());

	f.set_fit_type(new SlopeFit);
	f.fit();
	f.get_result().print( output );



	ASSERT_EQ( "RESULT y = 1 *x", output.str());
}

TEST( MainFunctionality, can_find_zero){

	std::stringstream input, output;
	input << "1 ; 0" << std::endl
		<< "2 ; 0 " << std::endl
		<< "3 ; 0 " << std::endl;



	DataReader d(Array::size);
	d.read_data( input );

	Fitter f;

	f.set_data(d.get());

	f.set_fit_type(new SlopeFit);
	f.fit();
	f.get_result().print( output );



	ASSERT_EQ( "RESULT y = 0 *x", output.str());
}



TEST( MainFunctionality, can_handle_no_input){

	std::stringstream input, output;

	ASSERT_ANY_THROW( {

			DataReader d(Array::size);
			d.read_data( input );

			Fitter f;

			f.set_data(d.get());

			f.set_fit_type(new SlopeFit);
			f.fit();
			f.get_result().print( output );

		});

}

TEST( MainFunctionality, DISABLED_yeat_another_test){

	std::stringstream input, output;
	input << "0; 0" << std::endl;



	DataReader d(Array::size);
	d.read_data( input );

	Fitter f;

	f.set_data(d.get());

	f.set_fit_type(new SlopeFit);
	f.fit();
	f.get_result().print( output );



	ASSERT_EQ( "RESULT y = 0 *x", output.str());
}






TEST( MainFunctionality, DISABLED_basic_fitting_for_linear_fitter){

	std::stringstream input, output;
	input << "1 ; 1" << std::endl
		<< "2 ; 2 ";



	DataReader d(Array::size);
	d.read_data( input );

	Fitter f;

	f.set_data(d.get());

	f.set_fit_type(new LinearFit);
	f.fit();
	f.get_result().print( output );



	ASSERT_EQ( "RESULT y = 1 *x", output.str());
}



