#pragma once

#include <iostream>

class Result{

	int _a;

	public:
		Result ( int a):
			_a(a){};
		void print ( std::ostream& ostream );


		bool operator== ( const Result& an) const{
			return _a == an._a;
		}


};


