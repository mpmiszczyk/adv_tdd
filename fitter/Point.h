#pragma once

#include <string>
#include <sstream>
#include <exception>

class Point{


	public:
		static Point FromLine( std::string line){

			std::istringstream istr ( line );
			int x,y;
			char del;

			istr >> x >> del >> y;
			if ( del != ';' )
				throw BadFormat ( "Bad formatted line: " + line );


			return Point(x,y);
		};

		int _x, _y;


	public:


		Point (int x, int y):
			_x(x),
			_y(y) {};


		bool operator== (const Point & another) const{
			return _x == another._x && _y == another._y;
		}
		bool operator!= (const Point & another) const{
			return ! operator==( another);
		}

		class BadFormat: public std::exception {
			std::string _msg;
			public:
				BadFormat ( std::string msg):
					_msg(msg){};

				const char* what() throw() {
					return _msg.c_str();
				}

				~BadFormat() throw() {};
		};

};
