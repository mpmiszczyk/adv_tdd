#include "Fitter.h"

Fitter::Fitter():
	_fit(NULL),
	_result(NULL){};

Fitter::~Fitter(){
	delete _fit;
	delete _result;
}


void Fitter::set_fit_type( Fit* fit ){
				delete _fit;
				_fit = fit;
};


void Fitter::set_data( std::list<Point> data ){
	_data = data;
}


void Fitter::fit(){
	delete _result;
	_result = _fit->fit( _data);
};


Result Fitter::get_result(){
	return *_result;
};
