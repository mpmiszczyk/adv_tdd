#pragma once

#include "Fit.h"

class SlopeFit: public Fit {
	public:
		virtual Result* fit( std::list<Point> points){

			if ( points.size() > 0 ) {

				int licznik = 0, mianownik = 0;


				for ( auto point = points.begin(); point != points.end(); point++){
					licznik += point->_x * point->_y;
					mianownik += point->_x * point->_x;
				}

				return new Result( licznik / mianownik );


			} else {
				throw NotEnoughData();
			}
		}


		class NotEnoughData: std::exception{
			public:
				virtual ~NotEnoughData() throw() {};

		};
};
