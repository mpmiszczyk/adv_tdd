#include "gtest/gtest.h"

#include "AutoChecker.h"

TEST ( Test, that ){
	
	try{
		typedef auto_ptr<int,NotNullCheck<int> > CheckPtr;
		CheckPtr p1(new int(5)), p2(new int(8));
		cout << "p1 = " << *p1 << endl << "p2 = " << *p2 << endl;
		CheckPtr p3 = p2; 
		cout << "=========== Przypisanie wskaznikowi p3, wartosci wskaznia p2 ============" << endl;
		cout << "p1 = " << *p1 << endl;
		cout << "p3 = " << *p3 << endl;
		cout << "p2 = " << *p2 << endl;// rzuca wyjatek
	}   
	catch(NullPointerException & e){ 
		cerr << "Wstapil blad, jego przyczyna to:" << endl;
		cerr << e.what() << endl;
	}   
	cout << endl << endl; 
	try{
		typedef auto_ptr<int,NoCheck<int> > NoCheckPtr;
		NoCheckPtr p1(new int(5)), p2(new int(8));
		cout << "p1 = " << *p1 << endl << "p2 = " << *p2 << endl;
		NoCheckPtr p3 = p2; 
		cout << "=========== Przypisanie wskaznikowi p3, wartosci wskaznia p2 ============" << endl;
		cout << "p1 = " << *p1 << endl;
		cout << "p3 = " << *p3 << endl;
		*p2;
	}   
	catch(NullPointerException & e){ 
		cerr << "Wstapil blad, jego przyczyna to:" << endl;
		cerr << e.what() << endl;
	}   
}



TEST( TestNotNull, simple_assigment){
		typedef auto_ptr<int,NotNullCheck<int> > CheckPtr;
		CheckPtr p1(new int(5)), p2(new int(8));

		ASSERT_EQ( 5, *p1);
		ASSERT_EQ( 8, *p2);
}


TEST( TestNoCheck, simple_assigment){
		typedef auto_ptr<int,NoCheck<int> > NoCheckPtr;
		NoCheckPtr p1(new int(5)), p2(new int(8));

		ASSERT_EQ( 5, *p1);
		ASSERT_EQ( 8, *p2);
}




TEST( TestNotNull, reasigment_will_leve_p2_as_null){

		typedef auto_ptr<int,NotNullCheck<int> > CheckPtr;
		CheckPtr p1(new int(5)), p2(new int(8));

		CheckPtr p3 = p2; 

		ASSERT_EQ( 5, *p1);
		ASSERT_EQ( 8, *p3);
		ASSERT_THROW(  *p2;
				, NullPointerException) << "Juz proba dereferencji sprawi wywolanie wyjatka";
		//*p2;
}


TEST( TestNoCheck, reasigment_will_leve_p2_as_NOT_null){

		typedef auto_ptr<int,NoCheck<int> > CheckPtr;
		CheckPtr p1(new int(5)), p2(new int(8));

		CheckPtr p3 = p2; 

		ASSERT_EQ( 5, *p1);
		ASSERT_EQ( 8, *p3);

		ASSERT_NO_THROW( *p2; ) << "Dereferencja nie rzuca wyjątku, bo wskaźnik nie ma wartości NULL";
		ASSERT_DEATH_IF_SUPPORTED( std::cout << *p2; ,"" ) << "ale wskazywany powinien byc poza pamiecia dostepna procesowi";
		*p2;
		//std::cout << *p2;
}

