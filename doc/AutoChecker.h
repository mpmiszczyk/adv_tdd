/* 
	Wytyczne i klasy wytycznych

Autor: Bartosz Szewczyk
Data : 22.03.2013


 **/

#include <iostream>
#include <string>

using namespace std;

class NullPointerException : public exception {
	public:
		NullPointerException() : _s("Empty Pointer")
	{}
		const char * what()throw(){
			return _s.c_str();
		}
		virtual ~NullPointerException()throw()
		{}
	private:
		string _s;
};

template<typename T>
struct NotNullCheck {
	static void check(T * ptr) throw(NullPointerException){
		if(!ptr) throw NullPointerException();
	}
};

template<typename T>
struct NoCheck
{
	static void check(T *){ }
};

template<typename T, typename PtrCheck>
class auto_ptr : public PtrCheck{
	public:
		auto_ptr() : _p(0)
	{}
		auto_ptr(T * t): _p(0)
	{
		_p=t;
	}
		auto_ptr(const auto_ptr & copied) : _p(0)
	{
		_p=copied.getptr();
		copied.reset();
	}
		auto_ptr & operator=(const auto_ptr & copied){
			this.~auto_ptr();
			_p=*copied;
			copied.getptr() = 0;
		}
		T & operator*()const{
			PtrCheck::check(_p);
			return *_p;
		}   
		T * operator->(){
			PtrCheck::check(_p);
			return _p; 
		}   
		T * getptr() const{
			PtrCheck::check(_p);
			return _p; 
		}   
		void reset()const{
			_p = NULL;
		}   
		~auto_ptr(){
			if(_p)
				delete _p; 
		}   
	private:
		mutable T * _p; 
};


